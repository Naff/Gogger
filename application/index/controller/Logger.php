<?php
namespace app\index\controller;

use think\Request;

class Logger {
	public function index() {

	}

	public function insert() {
		$data = Request::instance()->getInput();
		$data = json_decode($data, true);

		if (($data['key'] ?? false) != md5(($data['salt'] ?? '') . 'gogger')) {
			return json(['code'=> 1, 'message'=> '签名错误']);
		}
		unset($data['key']);
		unset($data['salt']);
		$data['timestamp'] = time();
		model('Logger')->insert($data);

		return json(['code'=> 0, 'message'=> 'success']);
	}

	public function query() {

	}
}